<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<spring:url value="/resources/core/css/hello.css" var="coreCss" />
	<spring:url value="/resources/core/img/" var="coreImg" scope="property"></spring:url>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="${coreCss}" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style type="text/css">
		div{
			padding: 0px;
		}
	</style>
</head>
<body>
 <div id="menu_navegacion">
 	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- aca colocare un logo a futuro -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Home</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li>
      <form class="navbar-form navbar">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="name">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="password">
        </div>
        <button type="submit" class="btn btn-default">ingresar</button>
      </form>
      </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">opcion1</a></li>
            <li><a href="#">opcion1</a></li>
            <li><a href="#">opcion1</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">opcion1</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
 </div>
  <div>
	  <div id="myCarousel" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	    <li data-target="#myCarousel" data-slide-to="1"></li>
	    <li data-target="#myCarousel" data-slide-to="2"></li>
	  </ol>
	
	  <!-- imagenes del slide -->
	  <div class="carousel-inner" role="listbox">
	    <div class="item active">
	      <img src="<spring:url value="/resources/core/img/img2.jpeg"/>" alt="Los Angeles">
	    </div>
	
	    <div class="item">
	      <img src="<spring:url value="/resources/core/img/img1.jpg"/>" alt="Chicago">
	    </div>
	
	    <div class="item">
	      <img src="<spring:url value="/resources/core/img/img3.jpg"/>" alt="New York">
	    </div>
	  </div>
	
	  <!-- Left and right controls -->
	  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	    <span class="sr-only">anterior</span>
	  </a>
	  <a class="right carousel-control" href="#myCarousel" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	    <span class="sr-only">siguiente</span>
	  </a>
	 </div>
  </div>
  <div>
  	<a href="#" class="btn btn-default btn-lg disabled" role="button">Enlace</a>
  </div>
</body>
</html>
